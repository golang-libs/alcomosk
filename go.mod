module gitlab.com/golang-libs/alcomosk

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.16.0
)
