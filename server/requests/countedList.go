package requests

type CountedList struct {
	Count int64       `json:"count"`
	List  interface{} `json:"list"`
}

type Count struct {
	Count int64 `json:"count"`
}
