package requests

type DefaultResponse struct {
	Text interface{} `json:"result"`
	Code interface{} `json:"result_descr"`
}
