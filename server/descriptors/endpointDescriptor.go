package descriptors

import "github.com/gin-gonic/gin"

type EndpointDescriptor struct {
	URI        string
	SecCode    string
	HttpMethod string
	//Name                 string
	Chain []gin.HandlerFunc
	//DefaultAccessGroups  []string
	//MethodAccessOverride []string
	//Protected            bool
}
