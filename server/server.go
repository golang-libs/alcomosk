package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/golang-libs/alcomosk/server/descriptors"
)

var r *gin.Engine

type AdditionalInit func(engine *gin.Engine) error

func Init(additionalInit AdditionalInit) (err error) {
	r = gin.Default()

	if additionalInit != nil {
		err = additionalInit(r)
	}

	return err
}

func RegisterEndpoints(endpointGroups ...[]descriptors.EndpointDescriptor) {
	for _, eg := range endpointGroups {
		for _, e := range eg {
			r.Handle(e.HttpMethod, e.URI, e.Chain...)
		}
	}
}

func Start(port string) (err error) {
	return r.Run(":" + port)
}
