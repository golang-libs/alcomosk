package middleware

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/alcomosk/common"
	"gitlab.com/golang-libs/alcomosk/server/requests"
	"go.uber.org/zap"
	"net/http"
	"strings"
)

func panicToError(recover interface{}) (err error) {
	if recover != nil {
		switch x := recover.(type) {
		case string:
			err = errors.Errorf(x, "")
		case error:
			err = errors.Wrap(x, "Ошибка из паники")
		default:
			err = errors.Errorf("unknown panic", "")
		}
	}
	return
}

func isOKCode(code int) bool {
	return code >= http.StatusOK && code < http.StatusMultipleChoices
}

func formErrorResponse(isDebug bool, customCode int, customExtCode interface{}, customText interface{}, errMsgs []string) (int, []byte) {
	code := http.StatusInternalServerError
	text := interface{}("INTERNAL_ERROR")
	if isDebug {
		text = strings.Join(errMsgs, ",")
	}
	extCode := interface{}(-1)

	// Выставлять ОК при ошибке нельзя!
	if customCode != 0 && !isOKCode(customCode) {
		code = customCode
	}

	if customExtCode != 0 && customExtCode != nil {
		extCode = customExtCode
	}

	if customText != "" && customText != nil {
		text = customText
	}

	jsonErr := requests.DefaultResponse{Text: text, Code: extCode}
	responseBytes, _ := json.Marshal(&jsonErr)
	return code, responseBytes
}

func ErrHandler(isDebug bool) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				err := panicToError(r)
				zap.S().Error(err)
				// TODO
				maskedCode, b := formErrorResponse(isDebug, ctx.Writer.Status(), ctx.Keys[common.ExtCodeKey], ctx.Keys[common.MessageKey],
					ctx.Errors.Errors())
				ctx.Status(maskedCode)
				ctx.Header("Content-Type", "application/json")
				_, err = ctx.Writer.Write(b)
				if err != nil {
					zap.S().Error(err)
				}
			}
		}()

		ctx.Next()

		maskedCode := ctx.Writer.Status()
		var b []byte
		if len(ctx.Errors) > 0 || !common.IsOKCode(ctx.Writer.Status()) {
			maskedCode, b = formErrorResponse(isDebug, ctx.Writer.Status(), ctx.Keys[common.ExtCodeKey],
				ctx.Keys["Message"], ctx.Errors.Errors())

			zap.S().Errorf("ID: %v\n Errors: %s\n", ctx.Keys[common.IDKey], ctx.Errors.String())

			if !ctx.Writer.Written() {
				ctx.Data(maskedCode, "application/json", b)
			} else {
				ctx.Status(maskedCode)
			}

		}
	}
}
