package common

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

const IDKey = "ID"
const MessageKey = "Message"
const ExtCodeKey = "ExtCode"

func WriteCtxJSON(ctx *gin.Context, results interface{}) error {
	b, err := json.Marshal(results)
	if err != nil {
		return err
	}
	_, err = ctx.Writer.Write(b)
	if err != nil {
		return err
	}
	ctx.Writer.Header().Set("Content-Type", "application/json")
	return nil
}

func IsOKCode(code int) bool {
	// Либо задан НЕ ОК код, либо код вообще не задан
	return (code < http.StatusMultipleChoices && code >= http.StatusOK) || code == 0
}

func GetIntParamOrDefault(param string, def int) int {
	if param == "" {
		return def
	}

	temp, err := strconv.Atoi(param)
	if err != nil {
		return def
	}

	return temp
}

func GetInt64ParamOrDefault(param string, def int64) int64 {
	if param == "" {
		return def
	}

	temp, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		return def
	}

	return temp
}

func GetIntParam(param string) *int {
	if param == "" {
		return nil
	}

	temp, err := strconv.Atoi(param)
	if err != nil {
		return nil
	}

	return &temp
}

func GetBoolParam(param string) *bool {
	if param == "" {
		return nil
	}

	temp, err := strconv.ParseBool(param)
	if err != nil {
		return nil
	}

	return &temp
}

func GetBoolParamOrDefault(param string, def bool) bool {
	if param == "" {
		return def
	}

	temp, err := strconv.ParseBool(param)
	if err != nil {
		return def
	}

	return temp
}

func GetInt64Param(param string) *int64 {
	if param == "" {
		return nil
	}

	temp, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		return nil
	}

	return &temp
}

func GetStringParam(param string) *string {
	if param == "" {
		return nil
	}

	re, err := regexp.Compile(`['=+;:]`)
	if err != nil {
		return nil
	}

	param = re.ReplaceAllString(param, "")

	return &param
}

func GetTimeParam(param string) *time.Time {
	t, err := time.Parse("2006-01-02T15:04:05Z", param)
	if err != nil {
		return nil
	}

	return &t
}

func GetDateParam(param string) *time.Time {
	t, err := time.Parse("2006-01-02", param)
	if err != nil {
		return nil
	}

	return &t
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func RandomString(l int) string {
	bytes := make([]byte, l)
	rand.Seed(time.Now().UnixNano() - 100500)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes)
}

func GetMapByReqType(method string, b json.RawMessage) (m map[string]string, err error) {
	if method == http.MethodGet {
		m = map[string]string{}
		err = json.Unmarshal(b, &m)
		return
	}
	return
}
